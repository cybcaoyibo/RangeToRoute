﻿//IP range list to IP route list converter. By CYB.

#include <fstream>
#include <iostream>
#include <vector>
#include <sstream>
#include <boost/icl/interval_set.hpp>
#include <boost/algorithm/string.hpp>
#include <functional>

uint8_t acceptLongIPNum(const char *&ptr, size_t &remain) {
	int now = 0;
	for (int i = 0; i < 3; i++) {
		if (*ptr < '0' || *ptr > '9') {
			throw std::logic_error("digit expected");
		}
		now = 10 * now + (*ptr - '0');
		ptr++; remain--;
	}
	if (now > 0xff) throw std::logic_error("long ip num too big");
	return static_cast<uint8_t>(now);
}

template<char chr>
void acceptChar(const char *&ptr, size_t &remain) {
	if (*ptr != chr) throw std::logic_error(std::string{chr} + " expected");
	ptr++; remain--;
}

uint32_t parseLongIP(const std::string &src) {
	const char *ptr = src.c_str();
	size_t remain = src.size();
	uint32_t ip = ((uint32_t)acceptLongIPNum(ptr, remain)) << 24;
	acceptChar<'.'>(ptr, remain);
	ip |= ((uint32_t)acceptLongIPNum(ptr, remain)) << 16;
	acceptChar<'.'>(ptr, remain);
	ip |= ((uint32_t)acceptLongIPNum(ptr, remain)) << 8;
	acceptChar<'.'>(ptr, remain);
	ip |= acceptLongIPNum(ptr, remain);
	if (remain) throw std::logic_error("expect end of string");
	return ip;
}

std::string ipToString(uint32_t h) {
	std::ostringstream os;
	os << (h >> 24) << "." << (h << 8 >> 24) << "." << (h << 16 >> 24) << "." << (h << 24 >> 24);
	return os.str();
}

struct route {
	uint32_t ip;
	unsigned bits;
	operator std::string() const {
		return ipToString(ip) + "/" + std::to_string(bits);
	}
	uint32_t lowest() const {
		return ip;
	}
	uint32_t highest() const {
		uint32_t result = ip;
		auto bitsComp = 32 - bits;
		for (unsigned i = 0; i < bitsComp; i++) {
			result |= 1 << i;
		}
		return result;
	}
};

unsigned nCommonPrefix(uint32_t left, uint32_t right) {
	uint32_t xor = left ^ right;
	for (unsigned i = 0; i < 32; i++)
		if (xor << i >> 31)
			return i;
	return 32;
}

route routeFromIp(uint32_t ip, unsigned bits) {
	auto bitsComp = 32 - bits;
	return {ip >> bitsComp << bitsComp, bits};
}

template<typename T>
void pushVector(std::vector<T> &to, const std::vector<T> &from) {
	to.insert(to.end(), from.begin(), from.end());
}

std::vector<route> rangeToRoutes(uint32_t left, uint32_t right) {
	auto bits = nCommonPrefix(left, right);
	route now;
	while (true) {
		now = routeFromIp(left, bits);
		auto lowest = now.lowest();
		auto highest = now.highest();
		if (left == lowest && right == highest)
			return {now};
		if (left <= lowest && right >= highest)
			break;
		bits++;
	}
	auto lowest = now.lowest();
	auto highest = now.highest();
	std::vector<route> result;
	if (left < lowest)
		pushVector(result, rangeToRoutes(left, lowest - 1));
	result.push_back(now);
	if (right > highest)
		pushVector(result, rangeToRoutes(highest + 1, right));
	return std::move(result);
}

void readRange(std::string &ln, const std::function<void(uint32_t low, uint32_t high)> &cb) {
	boost::trim(ln);
	if (!ln.size()) return;
	auto pos = ln.find_first_of(" \t");
	if (pos == std::string::npos)
		throw std::logic_error("missing space in line");
	std::string left = ln.substr(0, pos);
	std::string right = ln.substr(pos + 1);
	pos = right.find_first_of(" \t");
	if (pos == std::string::npos)
		throw std::logic_error("missing second space in line");
	right = right.substr(0, pos);
	cb(parseLongIP(left), parseLongIP(right));
}

void readRanges(std::istream &stm, const std::function<void(uint32_t low, uint32_t high)> &cb) {
	uint32_t count = 1;
	while (true) {
		std::string ln;
		std::getline(stm, ln);
		if (stm.fail()) break;
		try {
			readRange(ln, cb);
		} catch (std::exception &e) {
			throw std::logic_error(std::string{"at line "} + std::to_string(count) + ": " + e.what());
		}
		count++;
	}
	std::cerr << "Got " << (count - 1) << " ranges" << std::endl;
}

uint32_t bitsToMask(unsigned bits) {
	uint32_t rst{};
	for (unsigned i = 0; i < bits; i++) {
		rst |= 1 << (31 - i);
	}
	return rst;
}

int main(int argc, char **args) {
	try {
		std::cerr << "IP range list to IP route list converter. By CYB." << std::endl;
		if (argc != 2)
			throw std::logic_error("wrong number of arguments");
		boost::icl::interval_set<uint32_t> ranges;
		readRanges(std::ifstream(args[1]), [&](uint32_t low, uint32_t high) {
			ranges += boost::icl::discrete_interval<uint32_t>(low, high, boost::icl::interval_bounds::closed());
		});
		uint32_t count{};
		for (const auto &i : ranges) {
			std::vector<route> routes = rangeToRoutes(i.lower(), i.upper());
			for (const route &j : routes) {
				std::cout << ipToString(j.ip) << " " << ipToString(bitsToMask(j.bits)) << std::endl;
			}
			count += routes.size();
		}
		std::cerr << "Output " << count << " routes";
	} catch (std::exception &e) {
		std::cerr << "Error:" << std::endl;
		std::cerr << e.what() << std::endl;
	}
	return 0;
}
